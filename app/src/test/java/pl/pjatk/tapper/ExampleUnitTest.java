package pl.pjatk.tapper;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

import org.junit.Test;

import static org.junit.Assert.*;

import android.support.test.espresso.matcher.ViewMatchers;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void greeterSaysHello() {
        onView(withId(R.id.main_menu))
                .perform(click())
                .check(matches(isDisplayed()));
//        onView(withId(R.id.greet_button)).perform(click());
//        onView(withText("Hello Steve!")).check(matches(isDisplayed()));
    }
}