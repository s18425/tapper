package pl.pjatk.tapper;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // for hiding top bar
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        Button newButton = (Button) findViewById(R.id.newbutton);
        Button exitButton = (Button) findViewById(R.id.exitbutton);
        Button topButton = (Button) findViewById(R.id.topbutton);
        newButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startGame();
            }
        });
        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        topButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showScores();
            }
        });
    }

    /**
     * Method that starts tha game after clicking button NEW GAME
     */
    public void startGame(){
        Intent intent = new Intent(this, GameActivity.class);
        startActivity(intent);

    }

    /**
     * Method that opens laderboard view after clicking button TOP
     */
    public void showScores(){
        Intent intent = new Intent(this, TopActivity.class);
        startActivity(intent);

    }
}