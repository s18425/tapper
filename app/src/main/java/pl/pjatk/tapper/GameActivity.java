package pl.pjatk.tapper;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class GameActivity extends AppCompatActivity {

    private GameView gameView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        // for hiding top bar
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        View view = findViewById(R.id.activityGame);
        gameView = (GameView) view.findViewById(R.id.gameView);
        gameView.setGameActivity(this);
    }

    /**
     * Method that is called to stop the cat game
     * and returns to the main menu of the game
     */
    @Override
    public void onPause() {
        super.onPause();
        gameView.stopGame(); // terminates the game
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }


}