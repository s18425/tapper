package pl.pjatk.tapper;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class TopActivity extends AppCompatActivity {
    /**
     * Listener bindings after creation
     */
    TextView topScore1, topScore2, topScore3;
    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top);
        SharedPreferences prefs =
                this.getSharedPreferences("myPrefsKey", Context.MODE_PRIVATE);
        Button backButton = (Button) findViewById(R.id.backbutton);
        topScore1 = (TextView) findViewById(R.id.topscore1);
        topScore2 = (TextView) findViewById(R.id.topscore2);
        topScore3 = (TextView) findViewById(R.id.topscore3);
        topScore1.setText(Integer.toString(prefs.getInt("score1", 0)));
        topScore2.setText(Integer.toString(prefs.getInt("score2", 0)));
        topScore3.setText(Integer.toString(prefs.getInt("score3", 0)));
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMenu();
            }
        });
    }

    /**
     * Go back to main menu
     */
    public void showMenu(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);

    }

}