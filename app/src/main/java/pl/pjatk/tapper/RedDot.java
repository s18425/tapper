package pl.pjatk.tapper;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.view.SurfaceView;

import java.io.InputStream;

public class RedDot {
    public boolean visible;

    private Bitmap bitmap;

    private int radius;

    private Matrix matrix;

    public Point point = new Point(-100,-100);

    private double timeVisible;

    /**
     * Constructor of the red dot that puts picture from file red_dot.png
     * and it creates Bitmap of the dot with properties
     * @param view parametr GameView that is needed for finding the needed picture
     *             in assets
     */
    public RedDot(SurfaceView view) {
        visible = false;
        matrix = new Matrix();

        InputStream ims = null;
        try {
            ims = view.getContext().getAssets().open("red_dot.png");
            bitmap = BitmapFactory.decodeStream(ims);
            bitmap = Bitmap.createScaledBitmap(
                    bitmap, 36, 36, false);
            radius = (int) bitmap.getWidth() / 2;
        } catch (Exception exc){
            System.out.println(exc);
        }
    }

    /**
     * Method that is called for updating the timer of visibility of red dot
     * it checks if dot is visible and then add interval's time to it.
     * if time of visibility reaches to 1 second, it removes it's visibility on screen
     * and resets the timer
     * @param interval time that is smaller than 1 second and is constantly added
     *                 to the point it reaches 1
     */
    public void update(double interval) {
        if(visible) {
            timeVisible += interval;

            if (timeVisible > 1) {
                timeVisible = 0;
                visible = false;
            }
        }
    }

    /**
     * Method that sets the point, where the red dot will appear, makes it
     * visible and resets the timer of time visibility to 0
     * @param point point, that is generated for red dot to appear
     */
    public void setPoint(Point point){
        visible = true;
        this.timeVisible = 0;
        this.point = point;
    }

    /**
     * Method that checks if bitmap was correctly created and draw the red dot picture on received canvas
     * if can't load the image it draws a small circle
     * @param canvas parametr where the game components are drawn when game is working
     *               that is needed to draw the red dot on it
     */
    public void drawDot(Canvas canvas) {
        if(bitmap == null) {
            canvas.drawCircle(point.x, point.y, radius, new Paint(Color.CYAN));
        }
        else {
            Paint paint = new Paint();
            paint.setColor(Color.RED);

            matrix.setScale(0.1f, 0.1f);
            matrix.setTranslate(point.x - radius, point.y - radius);
            canvas.drawBitmap(bitmap, matrix, paint);
        }
    }
}
