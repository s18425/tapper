package pl.pjatk.tapper;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;

public class GameView extends SurfaceView {
    private Activity activity;

    private boolean gameOver;

    private double timeLeft;
    private int hits;
    private GameThread gameThread;

    private GameActivity gameActivity;

    private SurfaceHolder holder;

    Drawable catPawDrawable = null;
    Drawable gameMenuDrawable = null;

    CatPaw catPaw;
    RedDot redDot;


    /**
     * Method is called when GameView is created to turn off the game after the game over popup
     * @param gameActivity GameActivity
     */
    public void setGameActivity(GameActivity gameActivity){
        this.gameActivity = gameActivity;
    }

    /**
     * GameView constructor that loads the game background image
     * and sets up the game and finally starts whole game thread
     * @param context basic context component
     * @param attrs basic attribute component
     */
    public GameView(Context context, AttributeSet attrs) {
        super(context, attrs);
        activity = (Activity) context;

        holder = getHolder();
        holder.addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                gameThread.setRunning(false);
            }
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                try
                {
                    InputStream ims = getContext().getAssets().open("cat_paw.png");
                    catPawDrawable = Drawable.createFromStream(ims, null);
                    ims.close();

                    ims = getContext().getAssets().open("main_background.png");
                    gameMenuDrawable = Drawable.createFromStream(ims, null);
                    ims.close();
                }
                catch(IOException ignored)
                {}

                if(catPawDrawable != null &&
                        gameMenuDrawable != null) {
                    newGame(); // set up and start a new game

                    gameThread = new GameThread(holder); // create thread
                    gameThread.setRunning(true); // start game running
                    gameThread.start(); // start the game loop thread
                }
                else {
                    showGameOverDialog();
                }
            }
            @Override
            public void surfaceChanged(SurfaceHolder holder, int format,
                                       int width, int height) {
            }
        });
    }



    /**
     * Method that starts new game that creates new cat's paw, new red dot,
     * resets the timer and score
     */
    public void newGame() {
        catPaw = new CatPaw(this);
        redDot = new RedDot(this);
        timeLeft = 20; // start the countdown at 20 seconds
        hits = 0;
        if (gameOver) { // start a new game after the last game ended

            stopGame();
        }

    }


    /**
     * Method that stops the game thread running when the game is over
     */
    public void stopGame() {
        if (gameThread != null)
            gameThread.setRunning(false); // tell thread to terminate
    }

    /**
     * Method that is drawing canvas and updating its times and hits
     * its called constantly in a thread and its checking if cat's paw and red dot
     * are visible so it still paints them on the canvas until they gone
     * @param canvas parameter received to draw components on it
     */
    private void drawGameElements(Canvas canvas) {
        canvas.drawColor(Color.WHITE);

        gameMenuDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        gameMenuDrawable.draw(canvas);

        Paint textPaintTime = new Paint(Color.BLACK);
        textPaintTime.setTextSize(60);
        textPaintTime.setFakeBoldText(true);
        textPaintTime.setAntiAlias(true);
        textPaintTime.setShadowLayer(20, 0, 0, Color.WHITE);
        canvas.drawText(getResources().getString(
                R.string.time_left, (int) timeLeft), 50, 100, textPaintTime);

        Paint textPaintHits = new Paint(Color.BLACK);
        textPaintHits.setTextSize(60);
        textPaintHits.setFakeBoldText(true);
        textPaintHits.setAntiAlias(true);
        textPaintHits.setShadowLayer(20, 0, 0, Color.WHITE);
        canvas.drawText(getResources().getString(
                R.string.hits, (int) hits), getViewWidth() - 270, 100, textPaintHits);

        if(redDot.visible){
            redDot.drawDot(canvas);
        }
        if(catPaw.visible){
            catPaw.drawPaw(canvas);
        }
    }
    private int getViewWidth() {
        return super.getWidth();
    }

    private int getViewHeight() {
        return super.getHeight();
    }

    /**
     * Method that is lowering the time left of the game based on received parameter
     * also cheks if time reached to 0 so it can stop the game and show score dialog.
     * It also updates time for cat's paw and red dot so they can know when to disappear from the screen.
     * It checks too it red dot is visible, if not it, the method
     * generates new position for red dot to appear on the screen
     * @param elapsedTimeMS calculated small time that passes while the game is running
     */
    private void updatePositions(double elapsedTimeMS) {
        ArrayList<Integer> listScores = new ArrayList<>(); // temp list
        SharedPreferences prefs =
                activity.getSharedPreferences("myPrefsKey", Context.MODE_PRIVATE);

        double interval = elapsedTimeMS / 1000.0; // convert to seconds

        timeLeft -= interval; // subtract from time left

        // update timer for cat-paw to disappear
        catPaw.update(interval);
        redDot.update(interval);
        if(!redDot.visible) {
            redDot.setPoint(new Point(
                    (int) Math.floor(Math.random() * (getViewWidth() - 0 + 1) + 0),
                    (int) Math.floor(Math.random() * (getViewHeight() - 0 + 1) + 0)));
        }
        // if the timer reached zero
        if (timeLeft <= 0) {
            timeLeft = 0;
            gameOver = true; // the game is over
            gameThread.setRunning(false); // terminate thread
            gameOver = false; // the game is not over

            listScores.add(prefs.getInt("score1", 0)); // add top1 to list
            listScores.add(prefs.getInt("score2", 0)); // add top2 to list
            listScores.add(prefs.getInt("score3", 0)); // add top3 to list
            listScores.add(hits); // add hits to list
            Collections.sort(listScores, Collections.reverseOrder()); // sort list

            prefs.edit().putInt("score1", listScores.get(0)).apply(); // save top 1
            prefs.edit().putInt("score2", listScores.get(1)).apply(); // save top 2
            prefs.edit().putInt("score3", listScores.get(2)).apply(); // save top 3
            showGameOverDialog(); // show the losing dialog
        }
    }

    /**
     * Method called then the time of game is over
     * is shows the popup on the screen with score
     */
    private void showGameOverDialog() {
        activity.runOnUiThread(
                () -> {
                    onButtonShowPopupWindowClick();
                }
        );
    }

    /**
     * Method that creates popup that is visible after game end's
     * it includes the hit score and demand the player to close it
     * with the button CLOSE
     */
    public void onButtonShowPopupWindowClick() {

        // inflate the layout of the popup window
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.activity_popup, null);

        // create the popup window
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true; // lets taps outside the popup also dismiss it
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, false);
        Button popupButton = (Button) popupView.findViewById(R.id.popupButton);
        TextView textViewScore = (TextView) popupView.findViewById(R.id.popupTextViewScore);
        textViewScore.setText(getResources().getString(
                R.string.taps, hits));
        popupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gameActivity.onPause();
            }
        });

        // show the popup window
        // which view you pass in doesn't matter, it is only used for the window tolken
        popupWindow.showAtLocation(getRootView(), Gravity.CENTER, 0, 0);

    }

    /**
     * Overridden method on a GameView for reading the point where the screen is touched
     * it makes the cat's paw visible on the screen and checks if the player hit the red dot
     * if there was a successes hit, it adds one hit point into the score
     * @param e parameter that gives the point values
     * @return basic true return
     */
    @Override
    public boolean onTouchEvent(MotionEvent e) {
        int action = e.getAction();

        // the user touched the screen or dragged along the screen
        if (action == MotionEvent.ACTION_DOWN) {
            // set the new cat paw location
            Point touchPoint = new Point((int) e.getX(),
                    (int) e.getY());

            // fire the Cat Paw
            catPaw.setPoint(touchPoint);

            // add a hit if the dot is below the currently calculated paw position
            if(Math.floor(Math.sqrt(Math.pow(Math.abs(e.getY() - redDot.point.y),2)
                    + Math.pow(Math.abs(e.getX() - redDot.point.x),2))) <= 90) {
                hits += 1;
            }
        }

        return true;
    }

    /**
     * Important class in the game that is a game thread
     * when thread is running, it gets the canvas and send it to method that draws elements.
     * There is a small time that is calculated and given to other components to know how much time passed
     *
     *
     */
    private class GameThread extends Thread {
        private final SurfaceHolder surfaceHolder; // for manipulating canvas
        private boolean threadIsRunning = true; // running by default

        // initializes the surface holder
        public GameThread(SurfaceHolder holder) {
            surfaceHolder = holder;
            setName("GameThread");
        }

        // changes running state
        public void setRunning(boolean running) {
            threadIsRunning = running;
        }

        // controls the game loop
        @Override
        public void run() {
            Canvas canvas = null; // used for drawing
            long previousFrameTime = System.currentTimeMillis();

            while (threadIsRunning) {
                try {
                    // get Canvas
                    canvas = surfaceHolder.lockCanvas(null);
                    // lock the surfaceHolder for drawing
                    synchronized(surfaceHolder) {
                        long currentTime = System.currentTimeMillis();
                        double elapsedTimeMS = currentTime - previousFrameTime;
                        updatePositions(elapsedTimeMS); // update game state

                        drawGameElements(canvas); // draw using the canvas
                        previousFrameTime = currentTime; // update previous time
                    }
                }
                finally {
                    if (canvas != null)
                        surfaceHolder.unlockCanvasAndPost(canvas);
                }
            }
        }
    }
}
